import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RNCamera } from 'react-native-camera';
// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as BarCodeActions } from 'store/ducks/barcodes';
// ui
import { Alert, Keyboard} from 'react-native';
import { LoaderModal } from 'components';
import {
  CameraContainer,
  MainContainer,
  BarInputContainer,
  BarInputTitle,
  BarInputBackground,
  BarInput,
  OkButton,
  OkButtonText,
} from './ScanStyle';

class Scan extends Component {
  state = {
    code: null,
    showLoaderModal: false,
  }

  async componentWillReceiveProps(nextProps) {
    const { errorSavingCode, savedCodeSuccess } = nextProps;
    const currentErrorStatus = this.props.errorSavingCode; // will prevent re render the success message
    if (errorSavingCode && errorSavingCode !== currentErrorStatus) {
      await this.closeModal();
      Alert.alert("Erreur lors de l'enregistrement du code!", errorSavingCode);
    }
    const currentSuccessStatus = this.props.savedCodeSuccess; // will prevent re render the success message
    if (savedCodeSuccess && savedCodeSuccess !== currentSuccessStatus) {
      console.log('im rendering');
      Alert.alert("Code enregistré avec succès!");
      await this.closeModal();
      this.setState({ code: null });
    }
  }

  onBarCodeFound = async (barcode) => {
    const { data } = barcode[0];
    if (data && data.length !== 13) {
      Alert.alert('Le code à barres doit avoir 13 chiffres!');
    } else {
      const { loading, saveNewCodeRequest } = this.props;
      const { showLoaderModal } = this.state;
      if (!loading && !showLoaderModal) {
        await this.setState({ showLoaderModal: true });
        saveNewCodeRequest(data);
      }
    }
  }

  onOkButtonPress = () => {
    Keyboard.dismiss();
    const { code } = this.state;
    const { saveNewCodeRequest } = this.props;
    if (code.length !== 13) {
      Alert.alert('Le code à barres doit avoir 13 chiffres!');
    } else {
      saveNewCodeRequest(code);
    }
  }

  closeModal = async () => {
    await setInterval(() => {
      this.setState({ showLoaderModal: false });
    }, 1000);
    return true;
  }

  render() {
    const TitleFirstLine = 'Scannez votre premier code barre';
    const TitleSecondLine = 'Ou saisisse directement le code';
    const SubmitText = 'OK';
    const { code, showLoaderModal } = this.state;
    return (
      <MainContainer>
        <RNCamera
          style={CameraContainer}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          permissionDialogTitle="Permission d'utiliser la caméra"
          permissionDialogMessage="Nous avons besoin de votre permission pour utiliser votre téléphone-appareil photo"
          onGoogleVisionBarcodesDetected={({ barcodes }) => this.onBarCodeFound(barcodes)}
        />
        <BarInputContainer>
          <BarInputTitle>{TitleFirstLine}</BarInputTitle>
          <BarInputTitle>{TitleSecondLine}</BarInputTitle>
          <BarInputBackground>
            <BarInput
              underlineColorAndroid="rgba(0,0,0,0)"
              placeholder="Saisissez votre code a 13 chiffres"
              keyboardType="number-pad"
              value={code}
              onChangeText={text => this.setState({ code: text })}
              maxLength={13}
              onSubmitEditing={Keyboard.dismiss}
            />
            <OkButton onPress={() => this.onOkButtonPress()}>
              <OkButtonText>{SubmitText}</OkButtonText>
            </OkButton>
          </BarInputBackground>
        </BarInputContainer>
        {
          showLoaderModal && (
            <LoaderModal closeModal={this.closeModal} />
          )
        }
      </MainContainer>
    );
  }
}

Scan.propTypes = {
  saveNewCodeRequest: PropTypes.func.isRequired,
  errorSavingCode: PropTypes.string,
  savedCodeSuccess: PropTypes.bool,
  loading: PropTypes.bool.isRequired,
};

Scan.defaultProps = {
  errorSavingCode: null,
  savedCodeSuccess: null,
};

const mapStateToProps = state => ({
  errorSavingCode: state.barcodes.errorSavingCode,
  savedCodeSuccess: state.barcodes.savedCodeSuccess,
  loading: state.barcodes.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(BarCodeActions, dispatch);

const ScanConnected = connect(mapStateToProps, mapDispatchToProps)(Scan);
export { ScanConnected as Scan };
