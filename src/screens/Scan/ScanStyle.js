import styled from 'styled-components/native';
import { colors, metrics } from 'styles';

export const CameraContainer = {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center',
};

export const MainContainer = styled.SafeAreaView`
  flex: 1;
  flex-direction: column;
  background-color: ${colors.black};
`;

export const BarInputContainer = styled.View`
  display: flex;
  width: ${metrics.BAR_INPUT_CONTAINER_WIDHT};
  height: ${metrics.BAR_INPUT_CONTAINER_HEIGHT};
  flex-direction: column;
  align-items: center;
  justify-content:center;
  background-color: ${colors.white};
  position: absolute;
  top: ${metrics.BAR_INPUT_CONTAINER_TOP_POSITION};
  left: ${metrics.BAR_INPUT_CONTAINER_LEFT_POSITION};
  border-radius: 2;
`;

export const BarInputTitle = styled.Text`
  font-size: 18;
  font-weight: 600;
  color: ${colors.secondary};
`;

export const BarInputBackground = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content:center;
  width: 86%;
  height: 64;
  background-color: ${colors.primary};
  margin-top: ${metrics.BASE_PADDING};
  padding: ${metrics.BASE_PADDING / 2}px;
`;

export const BarInput = styled.TextInput`
  flex: 0.8;
  font-size: 20;
  font-weight: 300;
  color: ${colors.white};
`;

export const OkButton = styled.TouchableOpacity`
  flex: 0.2;
  align-items: center;
  justify-content:center;
`;

export const OkButtonText = styled.Text`
  font-size: 20;
  font-weight: 900;
  color: ${colors.white};
`;
