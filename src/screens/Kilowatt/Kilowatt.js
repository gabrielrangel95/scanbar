import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getBatteryImage } from 'utils';
// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as BarCodeActions } from 'store/ducks/barcodes';
// ui
import {
  Container,
  MainScrollView,
  MainContainer,
  BluePointsTitle,
  GoldPointsTitle,
  BatteryImage,
  Subtitle,
  BatteryImageTouch,
} from './KilowattStyle';

class Kilowatt extends Component {
  componentDidMount() {
    const { getSavedDataRequest } = this.props;
    getSavedDataRequest();
  }

  onBlueBatteryPress = () => {
    const { addGoldBatteryRequest } = this.props;
    addGoldBatteryRequest();
  }

  render() {
    const { totalKw, blueBatteries, goldBatteries } = this.props;
    const blueBatteryImagePath = getBatteryImage('blue', blueBatteries);
    const goldBatteryImagePath = getBatteryImage('gold', goldBatteries);
    const blutPointsTitleString = `${totalKw} kW points`;
    const goldPointsTitleString = `VOS GOLDEN BATTERY: ${goldBatteries}`;
    const firstSubtitle = 'Atteignez 250 kW points supplémentaires';
    const secondSubtitle = "Remplissez 4 batteries bleues pour obtenir une batterie d'or";
    const thirdSubTitle = "Une abtteria d'or remplie doublera vos chaces pour gagner un voyage.Ces batteries sont cumulables et accroiteront vos chaces !";
    return (
      <MainContainer>
        <MainScrollView showsVerticalScrollIndicator={false}>
          <Container>
            <BluePointsTitle>{blutPointsTitleString}</BluePointsTitle>
            <Subtitle>{firstSubtitle}</Subtitle>
            <BatteryImageTouch onPress={() => this.onBlueBatteryPress()}>
              <BatteryImage source={blueBatteryImagePath} />
            </BatteryImageTouch>
            <GoldPointsTitle>{goldPointsTitleString}</GoldPointsTitle>
            <Subtitle>{secondSubtitle}</Subtitle>
            <BatteryImage source={goldBatteryImagePath} />
            <Subtitle>{thirdSubTitle}</Subtitle>
          </Container>
        </MainScrollView>
      </MainContainer>
    );
  }
}

Kilowatt.propTypes = {
  getSavedDataRequest: PropTypes.func.isRequired,
  addGoldBatteryRequest: PropTypes.func.isRequired,
  goldBatteries: PropTypes.number.isRequired,
  blueBatteries: PropTypes.number.isRequired,
  totalKw: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  goldBatteries: state.barcodes.goldBatteries,
  blueBatteries: state.barcodes.blueBatteries,
  totalKw: state.barcodes.totalKw,
});

const mapDispatchToProps = dispatch => bindActionCreators(BarCodeActions, dispatch);

const KilowattConnected = connect(mapStateToProps, mapDispatchToProps)(Kilowatt);
export { KilowattConnected as Kilowatt };
