import styled from 'styled-components/native';
import { colors, metrics } from 'styles';

export const MainContainer = styled.SafeAreaView`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  background-color: ${colors.white};
`;

export const MainScrollView = styled.ScrollView``;

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding-vertical: ${metrics.BASE_PADDING * 2}px;
  padding-horizontal: ${metrics.BASE_PADDING}px;
`;

export const BluePointsTitle = styled.Text`
  font-size: 24;
  font-weight: 700;
  color: ${colors.blue};
`;

export const GoldPointsTitle = styled.Text`
  font-size: 24;
  font-weight: 700;
  color: ${colors.gold};
  margin-top: ${metrics.BASE_PADDING * 2}px;
`;

export const Subtitle = styled.Text`
  font-size: 16;
  font-weight: 300;
  color: ${colors.secondary};
  margin-top: ${metrics.BASE_PADDING}px;
  text-align: center;
  max-width: ${metrics.SCREEN_WIDTH * 0.7};
`;

export const BatteryImageTouch = styled.TouchableOpacity`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const BatteryImage = styled.Image`
  width: ${metrics.BATTERY_IMAGE_WIDTH};
  height: ${metrics.BATTERY_IMAGE_HEIGHT};
  margin-top: ${metrics.BASE_PADDING}px;
`;
