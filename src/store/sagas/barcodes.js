import { put } from 'redux-saga/effects';
import { Creators as BarcodeActions } from 'store/ducks/barcodes';
import { AsyncStorage } from 'react-native';

const STORA_DATA_KEY = '@ScanBar:data';
const GOLD_BATTERY_DATA_KEY = '@ScanBar: goldBattery';

const saveCodeAtDabase = async (data) => {
  const convertedData = JSON.stringify(data); // convert the data for save
  // save and return the result
  const savedResult = await AsyncStorage.setItem(STORA_DATA_KEY, convertedData).then(() => true)
    .catch(err => err);
  return savedResult;
};

export function* addGoldBattery() {
  try {
    // see if haves a db
    const numberOfGoldBatteries = yield AsyncStorage.getItem(GOLD_BATTERY_DATA_KEY);
    if (numberOfGoldBatteries) { // if have
      const numberOfGoldBatteriesConverted = JSON.parse(numberOfGoldBatteries);
      const newNumberOfBatteries = numberOfGoldBatteriesConverted + 1; // add 1 to the previous number
      yield AsyncStorage.setItem(GOLD_BATTERY_DATA_KEY, JSON.stringify(newNumberOfBatteries)); // save
      yield put(BarcodeActions.addGoldBatterySuccess()); // call success
      yield put(BarcodeActions.getSavedDataRequest()); // update data, for use on Kilowatt
    } else {
      yield AsyncStorage.setItem(GOLD_BATTERY_DATA_KEY, JSON.stringify(1)); // create a new db
      yield put(BarcodeActions.addGoldBatterySuccess()); // call success
      yield put(BarcodeActions.getSavedDataRequest()); // update data, for use on Kilowatt
    }
  } catch (err) {
    yield put(BarcodeActions.getSavedDataFailure("Erreur lors de la sauvegarde des données!"));
  }
}

export function* getData() {
  try {
    const data = yield AsyncStorage.getItem(STORA_DATA_KEY); // get data from db
    let goldBatteries = yield AsyncStorage.getItem(GOLD_BATTERY_DATA_KEY); // get number of golds
    if (!goldBatteries) { // if golds don't exists, it is 0
      goldBatteries = 0;
    } else {
      goldBatteries = JSON.parse(goldBatteries);
    }
    if (data) {
      const dataTransformed = JSON.parse(data); // transform the data for an array
      const blueBatteries = dataTransformed.length - (10 * goldBatteries); // get the blue number
      const totalKw = 25 * dataTransformed.length;
      // call success
      yield put(BarcodeActions.getSavedDataSuccess(dataTransformed, goldBatteries, blueBatteries, totalKw));
    }
  } catch (err) {
    yield put(BarcodeActions.getSavedDataFailure("Erreur lors de la récupération des données!"));
  }
}

export function* saveNewCode(action) {
  try {
    const { code } = action.payload; // code that the user entered
    // will get from dabase the array of codes entered
    const data = yield AsyncStorage.getItem(STORA_DATA_KEY);
    if (data) { // if this data exists
      const dataTransformed = JSON.parse(data); // transform the JSON to a Array
      if (dataTransformed.includes(code)) { // if the code is inside the array of savedes
        yield put(BarcodeActions.saveNewCodeFailure(`Ce code a déjà été enregistré`)); // will call the error
      } else {
        const myNewData = [...dataTransformed]; // clone the data
        myNewData.push(code); // add the code
        const saveNewDataResult = yield saveCodeAtDabase(myNewData); // save at db
        if (saveNewDataResult) { // if saved
          yield put(BarcodeActions.saveNewCodeSuccess()); // call the success
          yield put(BarcodeActions.getSavedDataRequest()); // call get data, for update Kilowatt
        } else {
          yield put(BarcodeActions.saveNewCodeFailure("Erreur lors de la sauvegarde des données!"));
        }
      }
    } else { // we dont have a database yet
      // save a new array, containing the code
      console.log('saving new db');
      const saveNewDataResult = yield saveCodeAtDabase([code]);
      if (saveNewDataResult) { // if saved
        yield put(BarcodeActions.saveNewCodeSuccess()); // call the success
        yield put(BarcodeActions.getSavedDataRequest()); // call get data, for update Kilowatt
      } else {
        yield put(BarcodeActions.saveNewCodeFailure("Erreur lors de la sauvegarde des données!"));
      }
    }
  } catch (err) {
    console.log(err);
    yield put(BarcodeActions.saveNewCodeFailure("Erreur lors de la sauvegarde des données!"));
  }
}
