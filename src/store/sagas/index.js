import { all, takeLatest } from 'redux-saga/effects';
import { Types } from '../types';

import { saveNewCode, getData, addGoldBattery } from './barcodes';

export default function* rootSaga() {
  return yield all([
    takeLatest(Types.SAVE_NEW_CODE_REQUEST, saveNewCode),
    takeLatest(Types.GET_SAVED_DATA_REQUEST, getData),
    takeLatest(Types.ADD_NEW_GOLD_BATTERY_REQUEST, addGoldBattery),
  ]);
}
