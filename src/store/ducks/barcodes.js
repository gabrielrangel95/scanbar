import { Types } from '../types';

const initialState = {
  data: [],
  errorGettingData: null,
  loading: false,
  goldBatteries: 0,
  blueBatteries: 0,
  totalKw: 0,
  errorSavingCode: null,
  savedCodeSuccess: null,
  errorAddingGoldBattery: null,
  addedGoldBattery: null,
};

export default function barcodes(state = initialState, action) {
  switch (action.type) {
    case Types.SAVE_NEW_CODE_REQUEST:
      return {
        ...state,
        loading: true,
        errorSavingCode: null,
        savedCodeSuccess: null,
      };
    case Types.SAVE_NEW_CODE_SUCCESS:
      return {
        ...state,
        errorSavingCode: null,
        loading: false,
        savedCodeSuccess: true,
      };
    case Types.SAVE_NEW_CODE_FAILURE:
      return {
        ...state,
        errorSavingCode: action.payload.error,
        loading: false,
        savedCodeSuccess: false,
      };
    case Types.GET_SAVED_DATA_REQUEST:
      return {
        ...state,
        loading: true,
        errorGettingData: null,
      };
    case Types.GET_SAVED_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        goldBatteries: action.payload.goldBatteries,
        blueBatteries: action.payload.blueBatteries,
        totalKw: action.payload.totalKw,
        errorGettingData: null,
      };
    case Types.GET_SAVED_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        errorGettingData: action.payload.error,
      };
    case Types.ADD_NEW_GOLD_BATTERY_REQUEST:
      return {
        ...state,
        loading: true,
        addedGoldBattery: null,
        errorAddingGoldBattery: null,
      };
    case Types.ADD_NEW_GOLD_BATTERY_SUCCESS:
      return {
        ...state,
        loading: false,
        addedGoldBattery: true,
        errorAddingGoldBattery: null,
      };
    case Types.ADD_NEW_GOLD_BATTERY_FAILURE:
      return {
        ...state,
        loading: false,
        addedGoldBattery: false,
        errorAddingGoldBattery: action.payload.error,
      };
    default:
      return state;
  }
}

export const Creators = {
  saveNewCodeRequest: code => ({
    type: Types.SAVE_NEW_CODE_REQUEST,
    payload: {
      code,
    },
  }),
  saveNewCodeSuccess: () => ({
    type: Types.SAVE_NEW_CODE_SUCCESS,
  }),
  saveNewCodeFailure: error => ({
    type: Types.SAVE_NEW_CODE_FAILURE,
    payload: {
      error,
    },
  }),
  getSavedDataRequest: () => ({
    type: Types.GET_SAVED_DATA_REQUEST,
  }),
  getSavedDataSuccess: (data, goldBatteries, blueBatteries, totalKw) => ({
    type: Types.GET_SAVED_DATA_SUCCESS,
    payload: {
      data,
      goldBatteries,
      blueBatteries,
      totalKw,
    },
  }),
  getSavedDataFailure: error => ({
    type: Types.GET_SAVED_DATA_FAILURE,
    payload: {
      error,
    },
  }),
  addGoldBatteryRequest: () => ({
    type: Types.ADD_NEW_GOLD_BATTERY_REQUEST,
  }),
  addGoldBatterySuccess: () => ({
    type: Types.ADD_NEW_GOLD_BATTERY_SUCCESS,
  }),
  addGoldBatteryFailure: error => ({
    type: Types.ADD_NEW_GOLD_BATTERY_FAILURE,
    payload: {
      error,
    },
  }),
};
