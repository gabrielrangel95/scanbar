import { Dimensions } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const BASE_PADDING = 16;

// SCAN SCREEN
// BAR INPUT SIZES:
export const BAR_INPUT_CONTAINER_WIDHT = SCREEN_WIDTH * 0.94;
export const BAR_INPUT_CONTAINER_HEIGHT = SCREEN_HEIGHT * 0.3;
export const BAR_INPUT_CONTAINER_TOP_POSITION = SCREEN_HEIGHT * 0.06;
export const BAR_INPUT_CONTAINER_LEFT_POSITION = SCREEN_WIDTH / 2 - BAR_INPUT_CONTAINER_WIDHT / 2;

// KILOWATTS SCREEN
export const BATTERY_IMAGE_WIDTH = SCREEN_WIDTH * 0.7;
export const BATTERY_IMAGE_HEIGHT = BATTERY_IMAGE_WIDTH * 0.373;
