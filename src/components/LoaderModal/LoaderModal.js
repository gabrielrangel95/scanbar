import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-native';
import { colors } from 'styles';
import {
  DataContainer,
  Loader,
  MainContainer,
  Text,
} from './LoaderModalStyle';

const LoaderModal = (props) => {
  const { closeModal } = props;
  return (
    <Modal
      visible
      transparent
      onRequestClose={closeModal}
    >
      <MainContainer>
        <DataContainer>
          <Loader size="large" color={colors.primary} />
          <Text>Chargement...</Text>
        </DataContainer>
      </MainContainer>
    </Modal>
  );
};

LoaderModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

export { LoaderModal };
