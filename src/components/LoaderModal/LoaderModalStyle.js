import styled from 'styled-components/native';
import { colors, metrics } from 'styles';

export const MainContainer = styled.SafeAreaView`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${colors.darkTransparent};
`;

export const DataContainer = styled.View`
  display: flex;
  width: ${metrics.SCREEN_WIDTH * 0.6};
  height: ${metrics.SCREEN_WIDTH * 0.6};
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 8;
  background-color: ${colors.white};
`;

export const Loader = styled.ActivityIndicator``;

export const Text = styled.Text`
  font-size: 18;
  font-weight: bold;
  color: ${colors.primary};
`;
