const emptyBlue = require('../assets/img/PILE_BLUE_00.png');
// blue battery
const blue1 = require('../assets/img/PILE_BLUE_50.png');
const blue2 = require('../assets/img/PILE_BLUE_100.png');
const blue3 = require('../assets/img/PILE_BLUE_150.png');
const blue4 = require('../assets/img/PILE_BLUE_200.png');
const blue5 = require('../assets/img/PILE_BLUE_250.png');
// const blueFull = require('../assets/img/PILE_BLUE_250_PLEINE.png');
// gold battery

const emptyYellow = require('../assets/img/PILE_YELLOW_00.png');
const yellow1 = require('../assets/img/PILE_YELLOW_250.png');
const yellow2 = require('../assets/img/PILE_YELLOW_500.png');
const yellow3 = require('../assets/img/PILE_YELLOW_750.png');
const yellow4 = require('../assets/img/PILE_YELLOW_1000.png');
// const yiellow_Full = require('../assets/img/PILE_YELLOW_1000_PLEINE_CTA.png');

export const getBatteryImage = (type, number) => {
  let returningPath = null;
  if (type === 'blue') {
    if (number < 2) {
      returningPath = emptyBlue;
    } else if (number <= 3) {
      returningPath = blue1;
    } else if (number <= 5) {
      returningPath = blue2;
    } else if (number <= 7) {
      returningPath = blue3;
    } else if (number <= 9) {
      returningPath = blue4;
    } else if (number >= 10) {
      returningPath = blue5;
    }
  } else if (number === 0) {
    returningPath = emptyYellow;
  } else if (number === 1) {
    returningPath = yellow1;
  } else if (number === 2) {
    returningPath = yellow2;
  } else if (number === 3) {
    returningPath = yellow3;
  } else if (number >= 4) {
    returningPath = yellow4;
  }

  return returningPath;
};
