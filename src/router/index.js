import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Kilowatt,
  Scan,
} from 'screens';
import { colors } from 'styles';

const Router = createBottomTabNavigator(
  {
    Scan: { screen: Scan },
    Kilowatt: { screen: Kilowatt },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Scan') {
          iconName = 'barcode-scan';
        } else if (routeName === 'Kilowatt') {
          iconName = 'battery-charging';
        }
        return <Icon name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: colors.primary,
      inactiveTintColor: colors.secondary,
    },
  },
);

export { Router };
