## Description:
This is a React Native App, where the users can scan a code bar (or input it) for charge batteries.
Each two bar code's insertions, a tab of the blue battery will be filled. When the blue battery is full,
if the user clicks on it, a tab of the gold battery will be filled.

## How to start:
As the babelsrc was changed for better imports on the app, it will be necessary start the react-native server clearing the cache, so first
is necessary run:
```
react-native start --reset-cache
```
then:
```
react-native run-ios 
or react-native run-android
```

## Tech specs
All the data is saved with AsynStorage, so it will persists even when the user close the app.
For style the components of the app, the styled-components package was used.
For detect the code bar, the react-native camera was used.
